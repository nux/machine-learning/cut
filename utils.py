import io
import os
import socket
import scatman
import functools
import tensorflow as tf
import numpy as np
from numpy.random import default_rng
import matplotlib.pyplot as plt
import seaborn as sns

sns.set_style("whitegrid")


def plot_to_image(figure):
    """Converts the matplotlib plot specified by 'figure' to a PNG image and
    returns it. The supplied figure is closed and inaccessible after this call."""
    # Save the plot to a PNG in memory.
    buf = io.BytesIO()
    plt.savefig(buf, format='png', dpi=72)
    # Closing the figure prevents it from being displayed directly inside
    # the notebook.
    plt.close(figure)
    buf.seek(0)
    # Convert PNG buffer to TF image
    image = tf.image.decode_png(buf.getvalue(), channels=4)
    # Add the batch dimension
    image = tf.expand_dims(image, 0)
    return image


def create_dir(folder):
    """ Create the directory.
    """
    if not os.path.exists(folder):
        os.makedirs(folder)
        print(f'Directory {folder} created')
    else:
        print(f'Directory {folder} already exists')

    return folder


@tf.function
def load_image(image_file, image_size=None, data_augmentation=True):
    """ Load the image file.
    """
    image = tf.io.read_file(image_file)
    image = tf.image.decode_png(image)
    image = (tf.cast(image, tf.float32) / 127.5) - 1.0

    if data_augmentation:
        image = tf.image.random_flip_left_right(image)
    if image_size is not None:
        image = tf.image.resize(image, size=(image_size[0], image_size[1]))
    if tf.shape(image)[-1] == 1:
        image = tf.tile(image, [1, 1, 3])

    return image


class ScatmanGenerator(object):
    def __init__(self,
                 bs,
                 wavelength,
                 angle,
                 resolution,
                 delta,
                 beta,
                 mean_radius,
                 degree=8,
                 gpus=0,
                 use_cpu=True,
                 seed=None,
                 **kwargs):
        """
        This class implements a Generator with infinite
        length for use with the PyScatman module.

        :param bs: The batch size
        :param wavelength: The wavelength of the irradiation
        :param angle: The maximal scattering angle
        :param resolution: Resolution in px of the output images
        :param delta: Real part of the refractive index (X-Ray Nomenclature: n'= 1 - delta)
        :param beta: Imaginary part of the refractive index
        :param mean_radius: Mean radius of the target particles in units of the wavelength
        :param degree: The max degree (l value) up to which spherical harmonics should be calculated
        :param gpus: Integer - or list of integers - specifying the GPUs to use
                     Ignored when use_cpu is True
        :param use_cpu: Boolean if the CPU should be used
        :param seed: The seed for the random generator
        :param kwargs: Additional keyword arguments
        """
        super(ScatmanGenerator, self).__init__(**kwargs)

        self.bs = bs
        self.wavelength = wavelength
        self.angle = angle
        self.resolution = np.round(resolution / np.cos(np.pi / 4)).astype(int)
        self.delta = delta
        self.beta = beta
        self.mean_radius = mean_radius
        self.degree = degree
        self.use_cpu = use_cpu
        self.seed = seed
        if self.use_cpu:
            scatman.use_cpu()
        else:
            scatman.use_gpu()
            scatman.set_gpu_ids(gpus)

        # Setting up the experiment
        scatman.set_experiment(wavelength=self.wavelength,
                               angle=self.angle,
                               resolution=self.resolution)
        # Initializing
        scatman.init()
        # defining the detector
        self.detector = scatman.Detectors.MSFT()
        # self.detector = scatman.Detectors.Ideal(mask_radius=int(.15 * self.resolution))
        # Initializing the random number generator
        self.rng = default_rng(seed=self.seed)
        scatman.info()

    def __iter__(self):
        return self

    # Python 3 compatibility
    def __next__(self):
        return self.next()

    @staticmethod
    def get_degree_indices(degree):
        """
        This return the indices for all l, m pairs that belong to a fixed l (the degree) value
        :param degree: the value for which the l, m pair indices should be returned
        :return: the indices
        """
        assert degree >= 0
        return np.arange(int(degree ** 2), int((1 + degree) ** 2)).astype(int)

    def get_power_spectrum(self, coeffs):
        """
        This returns the power spectrum for a given set of l, m coefficients
        :param coeffs: the l, m pairs, passed as a 1-d list
        :return: the power spectrum
        """
        ps_len = int(np.sqrt(len(coeffs)))
        ps = []
        for degree in range(1, 1 + ps_len):
            d_ind = self.get_degree_indices(degree)
            c_ = coeffs[d_ind]
            ps.append(np.sum(np.square(c_)))

        return np.array(ps)

    def next(self):
        # Orientation is uniformly distributed
        orientation_params = self.rng.uniform(0, 1, [self.bs, 2])
        # Size is log-normally distributed according to the Hagena scaling law
        # The sigma parameter of 0.25 is manually estimated based on the size distribution
        # in Phys. Rev. Lett. 121, 255301.
        size_params = self.rng.lognormal(np.log(self.mean_radius), .25, [self.bs, 1])
        # The coefficients cover all l, m pairs with l up to 8. lower l values are
        # exponentially more important than higher ones
        num_l_vals = int((self.degree + 1) ** 2 - 1)

        exp_weights_step = []
        exp_weights = np.exp(np.log(.1) / 10 * np.arange(self.degree + 1))
        for ll in range(self.degree + 1):
            exp_weights_step.extend([exp_weights[ll]] * len(self.get_degree_indices(ll)))

        coefficient_params = self.rng.uniform(0, 1, [self.bs, num_l_vals]) * np.array(exp_weights_step)[1:]
        shapes = [scatman.Shapes.SphericalHarmonics(
            a=s,
            # For example:  l up to 7 -> 64 parameter with exponentially decreasing importance. First val is always 1.
            coefficients=np.insert(c, 0, 1),
            latitude=o[0] * 180. - 90,  # Between -90° and 90°
            longitude=o[1] * 360.,  # Between 0° and 360°
            delta=self.delta,
            beta=self.beta) for o, s, c in zip(orientation_params,
                                               size_params,
                                               coefficient_params)]
        # Get the patterns
        patterns = self.detector.acquire(shapes)
        # Return everything
        # power_spectrums = [sg.get_power_spectrum(np.insert(c, 0, 1)) for c in coefficient_params]
        # Stack the parameter
        # params = np.hstack([orientation_params, size_params, power_spectrums])
        patterns_num = [np.expand_dims(x, -1) for x in patterns]
        return patterns_num


def center_cutout(img):
    # This cuts out the central part of the image so that
    # the height and width of the cropped area are fully
    # within the diffraction pattern.
    image = tf.image.central_crop(img, np.cos(np.pi / 4))
    return image


def build_target_dataset(args, is_training):
    def map_fn(ft):
        """Produces multiple transformations of the same batch."""
        return preprocess_fn(ft)

    mask = get_mask(args.image_size)
    preprocess_fn = get_preprocess_fn(args, cutout=True,
                                      mask=mask if args.mask_detector_hole.lower() == "true" else None)
    scatman_uncropped_image_size = np.round(args.image_size / np.cos(np.pi / 4)).astype(int)
    dataset = tf.data.Dataset.from_generator(ScatmanGenerator,
                                             output_signature=(
                                                 tf.TensorSpec(
                                                     shape=(args.batch_size, scatman_uncropped_image_size,
                                                            scatman_uncropped_image_size, 1),
                                                     dtype=tf.int32)),
                                             args=(args.batch_size,  # "bs"
                                                   args.wavelength,  # "wavelength"
                                                   args.angle,  # "angle"
                                                   args.image_size,  # "resolution"
                                                   args.delta,  # "delta"
                                                   args.beta,  # "beta"
                                                   args.mean_radius,  # "mean_radius"
                                                   8,  # "degree"
                                                   args.scatman_gpu if args.multi_gpu else 0,  # "gpus"
                                                   False)  # "use_cpu"
                                             )

    if is_training:
        options = tf.data.Options()
        options.experimental_deterministic = False
        options.experimental_slack = True
        dataset = dataset.with_options(options)
        buffer_multiplier = 50 if args.image_size <= 32 else 10
        dataset = dataset.shuffle(args.batch_size * buffer_multiplier)
        # dataset = dataset.repeat(-1)
    dataset = dataset.map(
        map_fn, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    # dataset = dataset.batch(args.batch_size, drop_remainder=is_training)
    dataset = dataset.prefetch(tf.data.experimental.AUTOTUNE)

    # input_fn = build_input_fn(dataset, args, is_training, keys)
    # return strategy.distribute_datasets_from_function(input_fn)
    return dataset


def build_source_dataset(args, is_training):
    def map_fn(ft):
        """Produces multiple transformations of the same batch."""
        pol_image = ft[keys[2]]
        return preprocess_fn(pol_image)

    mask = get_mask(args.image_size)
    preprocess_fn = get_preprocess_fn(args,
                                      mask=mask if args.mask_detector_hole.lower() == "true" else None)
    data_url = "https://share.phys.ethz.ch/~nux/datasets/{}.npz".format(args.source_dataset)
    if args.source_dataset == "static_helium_no_round_cart_all_meta":
        keys = ['bunch_id', 'cart_image', 'image', 'photon_energy', 'label', 'radius']
        file_keys = ['images', 'labels', 'cart_images', 'photon_energy', 'bunch_id', 'radius']
    else:
        keys = ['bunch_id', 'cart_image', 'image', 'photon_energy', 'photon_energies_categorical', "delays"]
        file_keys = ['images', 'cart_images', 'photon_energies', 'bunch_ids', 'photon_energies_categorical', "delays"]

    assert "dynamic_helium_no_round_cart_all_meta" in args.source_dataset or \
           "dynamic_helium_no_round_cart_all_meta_no_hole" in args.source_dataset or \
           "static_helium_no_round_cart_all_meta" in args.source_dataset

    if socket.gethostname() == "nux-noether":
        cache_dir = os.path.join("/", "scratch", "jzimmermann")
    else:
        cache_dir = "~/.keras"

    if args.source_dataset == "static_helium_no_round_cart_all_meta":
        md5hash = "e0a1ac6ec7b497ffc4a76e3bac2f8369"
    elif args.source_dataset == "dynamic_helium_no_round_cart_all_meta_no_hole":
        md5hash = "0798d7a8116675a0e2d26fafaf28a694"
    else:
        md5hash = "1f7a6c255c5ab0cc08a2c66486169cd5"
    path = tf.keras.utils.get_file(fname=args.source_dataset + ".npz",
                                   origin=data_url,
                                   md5_hash=md5hash,
                                   cache_dir=cache_dir
                                   )
    features = {}

    with np.load(path) as d:
        if args.source_dataset == "static_helium_no_round_cart_all_meta":
            features.update({keys[0]: d[file_keys[4]]})
            # features.update({keys[1]: d[file_keys[2]]})
            features.update({keys[2]: d[file_keys[0]]})
            # features.update({keys[3]: d[file_keys[3]]})
            # features.update({keys[4]: d[file_keys[1]]})
            # features.update({keys[5]: d[file_keys[5]]})
        else:
            features.update({keys[0]: d[file_keys[3]]})
            # features.update({keys[1]: d[file_keys[1]]})
            features.update({keys[2]: d[file_keys[0]]})
            # features.update({keys[3]: d[file_keys[2]]})
            # features.update({keys[4]: d[file_keys[4]]})
            # features.update({keys[5]: d[file_keys[5]]})
    dataset = tf.data.Dataset.from_tensor_slices(features)
    if args.cache_dataset.lower() == "true":
        dataset = dataset.cache()
    if is_training:
        options = tf.data.Options()
        options.experimental_deterministic = False
        options.experimental_slack = True
        dataset = dataset.with_options(options)
        buffer_multiplier = 50 if args.image_size <= 32 else 10
        dataset = dataset.shuffle(args.batch_size * buffer_multiplier)
        # dataset = dataset.skip(1250)  # just for testing
        # dataset = dataset.repeat(-1)
    dataset = dataset.map(
        map_fn, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    dataset = dataset.batch(args.batch_size, drop_remainder=is_training)
    dataset = dataset.prefetch(tf.data.experimental.AUTOTUNE)

    # input_fn = build_input_fn(dataset, args, is_training, keys)
    # return strategy.distribute_datasets_from_function(input_fn)
    return dataset


def get_preprocess_fn(args, cutout=False, mask=None):
    """Get function that accepts an image and returns a preprocessed image."""
    return functools.partial(
        preprocess_image,
        height=args.image_size,
        width=args.image_size,
        cutout=cutout,
        mask=mask)


def preprocess_image(image, height, width, cutout=False, mask=None):
    """Preprocesses the given image.

    Args:
      image: `Tensor` representing an image of arbitrary size.
      height: Height of output image.
      width: Width of output image.
      cutout: Should we use center crop
      cutout: if provided will be multiplied with the image before normalization
    Returns:
      A converted and resized image `Tensor` of range [-1, 1].
    """
    if cutout:
        image = center_cutout(image)

    image = tf.image.resize(image, [height, width], method=tf.image.ResizeMethod.BICUBIC)

    if mask is not None:
        image = tf.multiply(image, tf.cast(mask, image.dtype))

    image = normalize_img(image)

    return image


def get_mask(img_size):
    # Cut away the detector hole ... empirically derived
    mask_borders = np.round(img_size * np.array([94 / 224, 134 / 224, 101 / 224, 145 / 224])).astype(int)
    mask = np.ones([img_size, img_size, 1])
    mask[mask_borders[0]:mask_borders[1], mask_borders[2]:mask_borders[3]] = 0
    return mask


def normalize_img(x, new_max=1., new_min=-1.):
    """
    Linearly interpolate an image between "new_max" and "new_min"
    :param x: Input -> Will be casted to float32
    :param new_max: New maximum of x
    :param new_min: New minimum of x
    """

    x_cast = tf.cast(x, tf.float32)

    current_min = tf.math.reduce_min(x_cast)
    current_max = tf.math.reduce_max(x_cast)

    new_max_cast = tf.cast(new_max, tf.float32)
    new_min_cast = tf.cast(new_min, tf.float32)

    scaled_x = new_min_cast + (new_max_cast - new_min_cast) * (x_cast - current_min) / (
            current_max - current_min)

    return tf.cast(scaled_x, tf.float32)
