""" USAGE
python ./train.py --train_src_dir ./datasets/horse2zebra/trainA --train_tar_dir ./datasets/horse2zebra/trainB
        --test_src_dir ./datasets/horse2zebra/testA --test_tar_dir ./datasets/horse2zebra/testB
"""

import os
import json
import argparse
import datetime
import numpy as np
import tensorflow as tf
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
from glob import glob
from modules.cut_model import CUT_model
from utils import create_dir, build_source_dataset, build_target_dataset, normalize_img, plot_to_image

matplotlib.use("agg")
sns.set_style("whitegrid")


def ArgParse():
    # Parse command line arguments
    parser = argparse.ArgumentParser(description='CUT training usage.')
    # Training
    parser.add_argument('--mode', help="Model's mode be one of: 'cut', 'fastcut'", type=str,
                        default='cut',
                        choices=['cut', 'fastcut'])
    parser.add_argument('--epochs', help='Number of training epochs', type=int, default=100)
    parser.add_argument('--image_size', help='image size to which we rescale', type=int, default=224)
    parser.add_argument('--batch_size', help='Training batch size', type=int, default=1)
    parser.add_argument('--beta_1', help='First Momentum term of adam', type=float, default=0.5)
    parser.add_argument('--beta_2', help='Second Momentum term of adam', type=float, default=0.999)
    parser.add_argument('--wavelength', help='Wavelength used in the experiment in nm', type=float, default=45.)
    parser.add_argument('--angle', help='Maximum scattering angle of the detector in degrees', type=float, default=30)
    parser.add_argument('--delta', help="Real part of the refractive index (X-Ray Nomenclature: n'= 1 - delta)",
                        type=float, default=-0.27368)
    parser.add_argument('--beta', help='Imaginary part of the refractive index', type=float, default=0.702404)
    parser.add_argument('--mean_radius', help='Mean radius of the target particles in units of the wavelength',
                        type=float, default=800.)
    parser.add_argument('--degree',
                        help='The max degree (l value) up to which spherical harmonics should be calculated',
                        type=int, default=8)
    parser.add_argument('--lr', help='Initial learning rate for adam', type=float, default=0.0002)
    parser.add_argument('--lr_decay_rate', help='lr_decay_rate', type=float, default=0.9)
    parser.add_argument('--lr_decay_step', help='lr_decay_step', type=int, default=100000)
    parser.add_argument('--mask_detector_hole',
                        help='Should we mask the detector hole for source and target domain',
                        type=str, default="true", choices=["true", "false"])
    # Define data
    parser.add_argument('--out_dir', help='Outputs folder', type=str, default='./output')
    parser.add_argument('--source_dataset', help='The experimental dataset', type=str,
                        default='dynamic_helium_no_round_cart_all_meta_no_hole',
                        choices=["static_helium_no_round_cart_all_meta",
                                 "dynamic_helium_no_round_cart_all_meta",
                                 "dynamic_helium_no_round_cart_all_meta_no_hole"])
    parser.add_argument('--target_dataset', help='The simulated dataset', type=str,
                        default='scatman',
                        choices=["scatman"])  # Currently, only scatman simulations are accepted
    # Misc
    parser.add_argument('--ckpt', help='Resume training from checkpoint', type=str)
    parser.add_argument('--cache_dataset', help='Should we load the dataset into the cache',
                        type=str, default="false", choices=["true", "false"])
    parser.add_argument('--print_freq', help='Write to Log to disk every n batches', type=int, default=1000)
    parser.add_argument('--save_n_batches', help='Write CheckPoints every n batches', type=int, default=5000)
    parser.add_argument('--scatman_gpu', help='Idx of the GPU scatman should use', type=int, default=1)
    parser.add_argument('--impl', help="(Faster)Custom op use:'cuda'; (Slower)Tensorflow op use:'ref'", type=str,
                        default='ref', choices=['ref', 'cuda'])

    args = parser.parse_args()

    # Check arguments
    assert args.lr > 0
    assert args.epochs > 0
    assert args.batch_size > 0
    assert args.save_n_batches > 0

    return args


def main(args):
    def log_images_and_loss(batch, logs, num_img=5):
        if batch % args.print_freq == 0 and batch > 0:
            ep = len(glob(os.path.join(result_dir, "*step_{}.pdf".format(batch))))

            json_log.write(json.dumps({'epoch': ep, 'step': batch, 'D_loss': logs['D_loss'],
                                       'G_loss': logs['G_loss'], 'NCE_loss': logs['NCE_loss']}) + '\n')

            plt.close("all")
            norm_args = {"new_min": 0, "new_max": 1}
            f, ax = plt.subplots(num_img, 5, figsize=(8, int(2 * num_img)))

            for i, title in enumerate(['Source', "Translated", "Translated (Log)", "Target (Log)", "Identity (Log)"]):
                ax[0, i].set_title(title)

            for (source, target), a in zip(test_dataset.take(num_img), ax):
                translated = normalize_img(cut.netG(source)[0], **norm_args).numpy()
                source = normalize_img(source[0], **norm_args).numpy()
                idt = normalize_img(cut.netG(target)[0], **norm_args).numpy()
                target = normalize_img(target[0], **norm_args).numpy()

                a[0].imshow(source)
                a[1].imshow(translated)
                a[2].imshow(np.log(translated, where=translated > 0))
                a[3].imshow(np.log(target, where=target > 0))
                a[4].imshow(np.log(idt, where=idt > 0))

            for a in ax.flat:
                a.axis(False)
            f.tight_layout()
            f.savefig("{}/epoch_{}_step_{}.pdf".format(result_dir, ep, batch), dpi=72)

    # Create datasets
    train_dataset, test_dataset = create_dataset(args)

    # Get image shape
    # source_image, target_image = next(iter(train_dataset))
    source_shape = (args.image_size, args.image_size, 1)
    target_shape = (args.image_size, args.image_size, 1)

    # Create model
    print("Source Shape: {}\nTarget Shape: {}".format(source_shape, target_shape))
    with tf.device(args.gpus[0].name):
        cut = CUT_model(source_shape, target_shape, cut_mode=args.mode, impl=args.impl)

        # Define learning rate schedule
        lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(initial_learning_rate=args.lr,
                                                                     decay_steps=args.lr_decay_step,
                                                                     decay_rate=args.lr_decay_rate,
                                                                     staircase=True)

        # Compile model
        cut.compile(G_optimizer=tf.keras.optimizers.Adam(learning_rate=lr_schedule,
                                                         beta_1=args.beta_1,
                                                         beta_2=args.beta_2),
                    F_optimizer=tf.keras.optimizers.Adam(learning_rate=lr_schedule,
                                                         beta_1=args.beta_1,
                                                         beta_2=args.beta_2),
                    D_optimizer=tf.keras.optimizers.Adam(learning_rate=lr_schedule,
                                                         beta_1=args.beta_1,
                                                         beta_2=args.beta_2), )

        # Restored from previous checkpoints, or initialize checkpoints from scratch
        if args.ckpt is not None:
            latest_ckpt = tf.train.latest_checkpoint(args.ckpt)
            cut.load_weights(latest_ckpt)
            initial_epoch = int(latest_ckpt[-3:])
            print(f"Restored from {latest_ckpt}.")
        else:
            initial_epoch = 0
            print("Initializing from scratch...")

        # Create folders to store the output information
        base_path = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        result_dir = os.path.join(args.out_dir, base_path, "images")
        checkpoint_dir = os.path.join(args.out_dir, base_path, "checkpoints")
        log_dir = os.path.join(args.out_dir, base_path, "logs")

        for d in [result_dir, checkpoint_dir, log_dir]:
            create_dir(d)

        # Create checkpoint callback to save model's checkpoints every n epoch (default 5)
        # "period" to save every n epochs, "save_freq" to save every n batches
        checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(filepath=
                                                                 os.path.join(checkpoint_dir,
                                                                              "epoch_{epoch:02d}_step_{batch:02d}"),
                                                                 save_weights_only=True,
                                                                 save_freq=args.save_n_batches)

        # Create tensorboard callback to log losses every epoch
        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir,
                                                              histogram_freq=1,
                                                              write_graph=False,
                                                              update_freq="batch",
                                                              )
        json_log = open(os.path.join(log_dir, 'loss_log.json'), mode='wt', buffering=1)
        imgs_callback = tf.keras.callbacks.LambdaCallback(on_batch_end=log_images_and_loss,
                                                          on_train_end=lambda logs: json_log.close())

        # Train cut model
        cut.fit(train_dataset,
                epochs=args.epochs,
                initial_epoch=initial_epoch,
                callbacks=[imgs_callback,
                           checkpoint_callback,
                           tensorboard_callback],
                verbose=1)


def create_dataset(args):
    """ Create tf.data.Dataset.
    """

    train_src_dataset = build_source_dataset(args, is_training=True)
    train_tar_dataset = build_target_dataset(args, is_training=True)
    train_dataset = tf.data.Dataset.zip((train_src_dataset, train_tar_dataset))

    return train_dataset, train_dataset


if __name__ == '__main__':
    physical_devices = tf.config.list_physical_devices('GPU')
    if len(physical_devices) == 0:
        print("We need a GPU to perform training")
        exit()
    for pd in physical_devices:
        tf.config.experimental.set_memory_growth(pd, True)
    tf.config.set_soft_device_placement(True)
    args = ArgParse()

    logical_devices = tf.config.list_logical_devices('GPU')
    args.gpus = logical_devices
    if len(logical_devices) > 1:
        args.multi_gpu = True
    else:
        args.multi_gpu = False
    main(args)
